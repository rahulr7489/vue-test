import Vue from 'vue';
import VeeValidate from 'vee-validate';

import { store } from './_store';
import { router } from './_helpers';
import App from './app/App';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faDoorOpen,faIdCard,faBus,faRoute,faUser,faPowerOff, faCloud, faBusAlt, faCommentDots, faExclamationTriangle, faPlus, faBroadcastTower} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'


library.add(faDoorOpen,faIdCard,faBus,faRoute,faUser,faPowerOff,faCloud, faBusAlt, faCommentDots, faExclamationTriangle, faPlus, faBroadcastTower)
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.use(VeeValidate);
new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
});

