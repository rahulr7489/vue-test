const state = {
    all: {}
};
const user = JSON.parse(localStorage.getItem('user'));
const actions = {
    getAll({ commit }) {
        //commit('getAllRequest');
        console.log(this.state);
        const user = JSON.parse(localStorage.getItem('user'));
            if(user.username){
                commit('getAllSuccess', user)  
            } else {
                const error = "Data not found";
                commit('getAllFailure', error)
            }

    }
};

const mutations = {
    getAllRequest(state) {
        state.all = { loading: true };
    },
    getAllSuccess(state, users) {
        state.all = { items: users };
    },
    getAllFailure(state, error) {
        state.all = { error };
    }
};

export const users = {
    namespaced: true,
    state,
    actions,
    mutations
};
