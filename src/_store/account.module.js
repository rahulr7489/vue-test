//import { userService } from '../_services';
import { router } from '../_helpers';

const user = JSON.parse(localStorage.getItem('user'));
const state = user
    ? { status: { loggedIn: true }, user }
    : { status: {}, user: null };

const actions = {
    login({ dispatch, commit }, { username, password }) {
        //commit('loginRequest', { username });
        if(username === 'Admin' && password === 'admin1234'){
            const user = {username: username, loged :true}
            localStorage.setItem('user', JSON.stringify(user));
            commit('loginSuccess', user);
            router.push('/');

        } else {
            const error = "login Failed";
            commit('loginFailure', error);
            dispatch('alert/error', error, { root: true });
        }
    },
    logout({ commit }) {
        localStorage.removeItem('user');
        commit('logout');
    }
};

const mutations = {
    loginRequest(state, user) {
        state.status = { loggingIn: true };
        state.user = user;
    },
    loginSuccess(state, user) {
        state.status = { loggedIn: true };
        state.user = user;
    },
    loginFailure(state) {
        state.status = {};
        state.user = null;
    },
    logout(state) {
        state.status = {};
        state.user = null;
    }
};

export const account = {
    namespaced: true,
    state,
    actions,
    mutations
};